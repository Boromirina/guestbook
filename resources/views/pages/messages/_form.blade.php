@if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error  }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action='articleStore' id="id-form_messages">

    <div class="form-group">
        <label for="name">Username: <b>*</b></label>
        <input class="form-control"  name="name" type="text" id="name"  >
    </div>

    <div class="form-group">
        <label for="email">Email: <b>*</b></label>
        <input class="form-control" placeholder="name@example.com" name="email" type="email" id="email">
    </div>

    <div class="form-group">
        <label for="email">Website: </label>
        <input class="form-control" placeholder="http://example.com/" name="website" type="text" id="website">
    </div>

    <div class="form-group">
        <label for="name">Message: <b>*</b></label>
        <textarea class="form-control" rows='5'  name="message" id="message"></textarea>
    </div>

    <div class="form-group">
        {!! Recaptcha::Render() !!}
    </div>

        <button  class="btn btn-primary" type="submit">Add</button>

    {{ csrf_field() }}

</form>

