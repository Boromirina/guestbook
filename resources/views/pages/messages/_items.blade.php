<div class="container ">
    <table class="table table-bordered">
        <tr>
            <th class="table-info">#</th>
            <th class="table-info">@sortablelink('name',)</th>
            <th class="table-info">Email:</th>
            <th class="table-info">Website:</th>
            <th class="table-info">Message:</th>
            <th class="table-info">@sortablelink('created_at')></th>
        </tr>
        @if(!$messages->isEmpty())
            @foreach($messages as $message)
                <tr>
                    <td class="table-light">{{ $message->id }}</td>
                    <td class="table-light">{{ $message->name }}</td>
                    <td class="table-light">{{ $message->email }}</td>
                    <td class="table-light">{{ $message->website }}</td>
                    <td class="table-light">{{ $message->message }}</td>
                    <td class="table-light">{{ $message->created_at }}</td>
                </tr>
            @endforeach
        @endif

    </table>
             {{ $messages->appends(\Request::except('page'))->render() }}
</div>
