-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 31 2019 г., 15:27
-- Версия сервера: 10.1.38-MariaDB
-- Версия PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `guestbook`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `ip` varchar(255) NOT NULL,
  `explorer` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `website`, `message`, `ip`, `explorer`, `created_at`, `updated_at`) VALUES
(1, 'Olegs', 'olegsvolminskis@inbox.lv', '', 'Privet', '', NULL, '2019-05-07 21:00:00', '2019-05-16 21:00:00'),
(2, 'Olegs Volminskis', 'alla@bv.lv', '', 'aaaaa', '127.0.0.1', NULL, '2019-05-30 18:41:33', '2019-05-30 18:41:33'),
(3, 'Santa', 'santa@mail.ru', 'kinomonster.com', 'Message', '127.0.0.1', NULL, '2019-05-31 07:23:03', '2019-05-31 07:23:03'),
(4, 'Viktors', 'aaa@mail.ru', 'mihail@ya.ru', '0927301-@##%^&', '127.0.0.1', NULL, '2019-05-31 07:24:45', '2019-05-31 07:24:45'),
(5, 'Alla', 'masa@ls.com', 'rururur', 'ZXLKD&*()%$#@@~рпарпопрол', '127.0.0.1', NULL, '2019-05-31 08:43:49', '2019-05-31 08:43:49'),
(6, 'HALI-GALI', 'olegsvolminskis@inbox.lv', 'google@lv', 'hthththth', '127.0.0.1', NULL, '2019-05-31 10:08:18', '2019-05-31 10:08:18'),
(7, 'Sandra', 'sandra@mix.lv', 'hghghghgh@', 'dwsdsdsds', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-05-31 10:15:23', '2019-05-31 10:15:23'),
(8, 'grghrgrg', 'rgrgrgrg@cdcd.ru', 'dfdfdf', '6747698476094769', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-05-31 10:19:31', '2019-05-31 10:19:31'),
(9, 'maza', 'alex@ru.lv', 'zaazaza', 'zaazaadefegfe4g', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-05-31 10:20:49', '2019-05-31 10:20:49'),
(10, 'xxx', 'olegsvolminskis@inbox.lv', 'xxxx', 'rtrtrtrt', '127.0.0.1', '5.8.18', '2019-05-31 10:54:21', '2019-05-31 10:54:21'),
(11, 'ssas', 'olegsvolminskis@inbox.lv', 'sas@inbox.lv', 'asasa', '127.0.0.1', '74.0.3729', '2019-05-31 11:02:23', '2019-05-31 11:02:23'),
(12, 'ewewe', 'ewew@kk@ru', '\'kl;k;lk;lk;l', 'lkhlkhkjgkjgjk', '127.0.0.1', 'Chrome 74.0.3729', '2019-05-31 11:04:36', '2019-05-31 11:04:36'),
(13, 'ddsdsd', 'dsds@asas.lv', 'dsdsds', 'dsdsdsds', '127.0.0.1', 'Internet Explorer 11', '2019-05-31 11:07:20', '2019-05-31 11:07:20'),
(14, 'dfdfd', 'fdfdf@ii.lv', 's;dlj;lkj', ';lkjlkjlk', '127.0.0.1', '8.1', '2019-05-31 11:16:18', '2019-05-31 11:16:18'),
(15, 'wewewe', 'olegsvolminskis@inbox.lv', 'ewewe', 'wewewew', '127.0.0.1', 'Windows', '2019-05-31 11:17:15', '2019-05-31 11:17:15'),
(16, 'Olegs', 'olegsvolminskis@inbox.lv', 'rgtrgrg', 'grgrgrgr', '127.0.0.1', 'Windows', '2019-05-31 11:18:31', '2019-05-31 11:18:31'),
(17, 'cscs', 'csc@aa', 'dsds', 'sdsds', '127.0.0.1', 'Chrome 74.0.3729', '2019-05-31 11:24:42', '2019-05-31 11:24:42'),
(18, 'Santa', 'santux200@inbox.lv', NULL, 'Labdien! Man viss patik. Paldies', '127.0.0.1', 'Chrome 74.0.3729', '2019-05-31 12:10:45', '2019-05-31 12:10:45');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_05_28_121345_create_messages_table', 1),
(2, '2019_05_28_192521_create_tests_table', 2),
(3, '2019_05_29_201747_create_products_table', 3),
(4, '2019_05_30_124733_add_fields_messages_table', 4),
(5, '2019_05_30_213557_create_messages_table', 5);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
