<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Message extends Model
{
        use Sortable;
        
     protected $fillable = ['name', 'email', 'website', 'message', 'ip', 'explorer'];
     
     public $sortable = ['id', 'name' ,'email', 'website', 'message','created_at', 'updated_at'];
     

}
