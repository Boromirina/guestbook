<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;


class HomeController extends Controller
{
    public function index()
    {

        $dataMessage = [
                'title' => 'Guestbook',
                'pagetitle' => 'GuestBook',
                'messages' => Message::sortable()->latest()->paginate(10),
                'count' => Message::count()
                ];

        return view('pages.messages.index', $dataMessage);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
     {
        $this->validate($request,[

             'name' => 'required|max:255',
             'email' => 'required|email',
             'website' => 'nullable|url',
             'message' => 'required|max:500',
             'g-recaptcha-response' => 'required|recaptcha'

                                 ]);

             $ip =app()->Request::getClientIp();
             $useragent=app()->Browser::browserName();


             $data = $request->all();
             $data['explorer'] =$useragent;
             $data['ip'] =$ip;
             $message = new Message;
             $message->fill($data);
             $message->save();

     return redirect('/');
     }
        
}
  